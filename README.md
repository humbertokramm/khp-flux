# KHP-flux

Medidor de vasão para fluxo líquidos

Trabalho em grupo para a disciplina de instrumentação eletrônica

#### Componentes do grupo
- Patrício Adans Bischoff
- Humberto Correia Kramm
- Kamile Oliveira

### Descritivo:
A proposta do projeto é criar um medidor de fluxo para medir a passagem de água em tubulações residenciais permitindo ao morador realizar diagnósticos de vazamentos d’água e acompanhar os momentos do dia ou da semana em que ocorre maior consumo.
A proposta do sistema é realizar a transdução do fluxo d’água para sinal elétrico que pode ser sinais contínuos ou pulsos de acordo com o princípio de funcionamento do transdutor, ainda não definido. Se sabe que o transdutor não vai conseguir transmitir a informação de fluxo diretamente que será necessário o processamento do sinal levando em consideração a geometria da condutor do líquido. O sistema será composto pelo Transdutor excitado por uma Sinal de Alimentação Independente para dar estabilidade ao transdutor; Filtro de Entrada evitar interferências de sinais externos eletromagnéticos; Amplificador de Sinal realiza o ajuste de escada para entrada próximo estágio; Filtro de saída do Amplificador Sinal retira flutuações de origem elevado ganho amplificador; Conversor de sinal Analógico para Digital (ADC) converte sinal analógico para representação digital para ser processado pelo microcontrolador; Microcontrolador executa o algoritmo de tratamento sinal de entrada de acordo com as configurações convertendo para Unidade desejada; Display apresentado o valor da vazão e unidade; E Teclas permite o usuário mudar o modo de apresentação das informações e inserir configurações.


### Diagrama de Blocos
![Screenshot](Fig/Blocos.PNG)
Diagrama de blocos para sistema de Sensor de Fluxo.


### Benchmarking:

![Screenshot](Fig/tabela.PNG)

## Bibliotecas:

ESP32 OLED <- https://github.com/ThingPulse/esp8266-oled-ssd1306
