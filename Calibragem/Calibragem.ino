#include "SSD1306.h"

#include "Statistic.h"  // without trailing s
Statistic myStats;

SSD1306 display(0x3c, 5, 4);


const int sampleRate = 10-1;
const int Analog_channel_pin= A0;
const int Botao_pin= 25;
const int trigger_pin= 13;
int estado = 0;
int ADC_VALUE = 0;
float amostra;
float media;
int mediaInt;
long tempo;
float segundos;
int nAmostras = 0;
int buffer[50];


String adcValue;

void showValue(){
	display.clear();
	display.setFont(ArialMT_Plain_16);
	display.drawString(0, 0, "Leitura ADC [12b]");
	Serial.println("ADC ="+adcValue);
	display.setFont(ArialMT_Plain_24);
	display.drawString(0, 30, adcValue);

	display.display();
}

void getSensor(){
	static int index;
	static int temp=0;
	if(temp<millis()-sampleRate) temp = millis();
	else return;
	
	digitalWrite(trigger_pin,!digitalRead(trigger_pin));
	
	myStats.add(0xFFF - analogRead(Analog_channel_pin));
	
	if(index < 49){
		index++;
		return;
		}
	else {
		
		index = 0;
		amostra = int(myStats.average());
		myStats.clear();
	}
	
	switch(estado){
		case 0:
		media = amostra;
		tempo = millis();
		nAmostras = 1;
		mediaInt = int(media);
		adcValue = String(int(amostra))+"-"+String(mediaInt);
		break;
		
		case 1:
		media += amostra;
		nAmostras++;
		mediaInt = int(media/nAmostras);
		segundos = (millis()-tempo)/1000;
		if(segundos<10)adcValue = "  ";
		else if(segundos<100)adcValue = " ";
		else adcValue = "";
		adcValue += String(int(segundos))+":"+String(int(amostra));
		break;
		
		case 2:
		adcValue = String(int(segundos))+"?"+String(mediaInt);
		break;
	}
	
	showValue();
}



void getBotao(){
	static int temp=0;
	static int debounce=0;
	static bool estatoTecla=0;
	if(temp<millis()-10) temp = millis();
	else return;
	
	if(!digitalRead(Botao_pin)){
		if(estatoTecla == 0){
			debounce++;
			if(debounce >10){
				estatoTecla = 1;
				if(estado < 2) estado++;
				else estado = 0;
			}
		}
	}
	else{
		debounce=0;
		estatoTecla = 0;
	}
}

void setup() {
	pinMode(Botao_pin,INPUT_PULLUP);
	pinMode(trigger_pin,OUTPUT);
	
	Serial.begin(115200);
	display.init();
	display.clear();
}

void loop() {
	getSensor();
	getBotao();
}

