//Bibliotecas
#include "SSD1306.h"
#include "Statistic.h"
Statistic myStats;
SSD1306 display(0x3c, 5, 4);

//Pinos
const int Analog_channel_pin= A0;
const int Botao_pin= 25;
const int trigger_pin= 13;

//Constantes
const int sampleRate = 10-1;
const double calibragemA = 0.0029;
const double calibragemB = 1.5006;

//Variáveis
int estado = 0;
float amostra;
float media;
long tempo;
double flowValue;
double acumulado;
String adcValue;

//Mostra o valor no display e na serial
void showValue(){
	display.clear();
	display.setFont(ArialMT_Plain_16);
	display.drawString(0, 0, "KPH Flowmeter");
	Serial.println("ADC ="+adcValue);
	display.setFont(ArialMT_Plain_24);
	display.drawString(0, 30, adcValue);

	display.display();
}

//Cacula os Valores
void makeCalc(){
	switch(estado){
		//Calcula o Fluxo
		case 0:
		flowValue = amostra;
		if(flowValue>0){
			flowValue *= calibragemA;
			flowValue += calibragemB;
		}
		adcValue = String(flowValue)+" L/min";
		break;
		// Mostra o valor do AD
		case 1:
		adcValue = "AD: "+String(int(amostra));
		break;
		// Mostra o acumulado
		case 2:
		flowValue = amostra;
		if(flowValue>0){
			flowValue *= calibragemA;
			flowValue += calibragemB;
		}
		acumulado  += flowValue/120;
		adcValue = "T: "+ String(acumulado)+" L";
		break;
	}
}

//Obtem o valor do AD e calcula o fluxo
void getSensor(){
	static int index;
	static int temp=0;
	if(temp<millis()-sampleRate) temp = millis();
	else return;
	//Marca o momento da leitura da amostra
	digitalWrite(trigger_pin,!digitalRead(trigger_pin));
	//Armazena a amostra no buffer
	myStats.add(0xFFF - analogRead(Analog_channel_pin));
	//Retorna caso não tenha ainda as 50 amostras
	if(index < 49){
		index++;
		return;
	}
	// Calcula a média e limpa o buffer
	else {
		index = 0;
		amostra = int(myStats.average());
		myStats.clear();
	}
	makeCalc();  //Chama a função que faz os calculos
	showValue(); //Exbite o resultado no display e na Serial
}

// Obtem o estado
void getBotao(){
	static int temp=0;
	static int debounce=0;
	static bool estatoTecla=0;
	if(temp<millis()-10) temp = millis();
	else return;
	
	if(!digitalRead(Botao_pin)){
		if(estatoTecla == 0){
			debounce++;
			if(debounce >10){
				estatoTecla = 1;
				if(estado < 2) estado++;
				else estado = 0;
			}
		}
	}
	else{
		debounce=0;
		estatoTecla = 0;
	}
}

//Configura o sistema
void setup() {
	pinMode(Botao_pin,INPUT_PULLUP);
	pinMode(trigger_pin,OUTPUT);
	
	Serial.begin(115200);
	display.init();
	display.clear();
	tempo = millis();
}

//Loop infinito
void loop() {
	getSensor();
	getBotao();
}

